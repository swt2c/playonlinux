playonlinux (4.3.4-1) unstable; urgency=medium

  * New upstream version 4.3.4:
    + Fix display issues (Closes: #874284).
  * Standards-version: 4.3.0, no changes needed.
  * Adjust debian/playonlinux.install to the new upstream tree.

 -- Bertrand Marc <bmarc@debian.org>  Fri, 28 Dec 2018 14:53:06 +0100

playonlinux (4.3.3-1) unstable; urgency=medium

  * New upstream version 4.3.3
  * Update debian/watch to version 4 and use https.
  * Standards-version: 4.2.1, no changes needed.
  * debian/control:
    + Use https in the homepage field.
    + Reorder and move dependencies.
    + Add a dependency on jq.
    + Depends on wine32|wine, and not wine-development, as it doesn't provide
      /usr/bin/wine anymore.
    + Suggests winbind.
  * Move the package to salsa and update Vcs-Git and Vcs-browser accordingly.
  * debian/copyright: update years.

 -- Bertrand Marc <bmarc@debian.org>  Sat, 24 Nov 2018 19:02:52 +0100

playonlinux (4.2.12-1) unstable; urgency=medium

  * Use my @debian.org address for the maintainer field and in debian/copyright.
  * Moved the package to Git.
  * New upstream version 4.2.12 (Closes: #863711, #849119).
  * debian/rules: override dh_auto_install to disable upstream's Makefile.
  * debian/rules: remove override_dh_install, not needed anymore.
  * Use the manpages shipped upstream:
    + remove debian/man.
    + update debian/playonlinux.manpages.
    + add a patch to fix a typo in playonlinux.1.
  * Use the desktop file shipped upstream (Closes: #845548).
  * Update years and Format: url in debian/copyright.
  * Standards version: 4.0.0.

 -- Bertrand Marc <bmarc@debian.org>  Wed, 26 Jul 2017 11:14:57 +0200

playonlinux (4.2.10-2) unstable; urgency=medium

  * Remove obsolete plugins from debian/playonlinux.install (Closes: #820329).
  * Standards version 3.9.8: no changes needed.
  * Update years in debian/copyright.

 -- Bertrand Marc <beberking@gmail.com>  Sat, 09 Apr 2016 15:55:40 +0200

playonlinux (4.2.10-1) unstable; urgency=medium

  * New upstream release.

 -- Bertrand Marc <beberking@gmail.com>  Sun, 31 Jan 2016 16:05:04 +0100

playonlinux (4.2.9-2) unstable; urgency=medium

  * Team upload.

  [ Markus Koschany ]
  * Use compat level 9 and require debhelper >= 9.
  * Vcs-Browser: Use https.
  * d/control: Remove superfluous ${shlibs:Depends} substvar.
  * Remove dependency on wine32|wine32-development again to ensure the package
    can migrate to testing.

  [ Bertrand Marc ]
  * Remove debian/menu.

 -- Markus Koschany <apo@debian.org>  Fri, 08 Jan 2016 10:47:57 +0100

playonlinux (4.2.9-1) unstable; urgency=medium

  * New upstream release.

 -- Bertrand Marc <beberking@gmail.com>  Sat, 19 Sep 2015 13:47:37 +0200

playonlinux (4.2.8-1) unstable; urgency=medium

  * New upstream release.
  * Update debian/copyright to match the new tree.
  * Depend on wine32|wine32-development to make sure the 32 bits version
    of wine is installed. POL assumes it is (Closes: #783875).
  * Build-depend on dh-python as it is used in the build process.

 -- Bertrand Marc <beberking@gmail.com>  Sat, 18 Jul 2015 12:14:57 +0200

playonlinux (4.2.6-1) unstable; urgency=medium

  * New upstream release.
  * Refresh patches.
  * Standards version 3.9.6: no changes needed.
  * Update debian/watch to https. Upstream disabled http.

 -- Bertrand Marc <beberking@gmail.com>  Sun, 29 Mar 2015 16:21:48 +0200

playonlinux (4.2.5-1) unstable; urgency=medium

  * New upstream release.
  * Refresh patches.
  * Depends on python-wxgtk3.0 instead on python-wxgtk2.8 (Closes: #757429).

 -- Bertrand Marc <beberking@gmail.com>  Fri, 12 Sep 2014 23:25:33 +0200

playonlinux (4.2.4-1) unstable; urgency=medium

  * Team upload.

  [ Bertrand Marc ]
  * New upstream release.

  [ Vincent Cheng ]
  * Replace depends on wine-unstable with wine-development. (Closes: #758313)

 -- Bertrand Marc <beberking@gmail.com>  Tue, 12 Aug 2014 19:51:48 +0200

playonlinux (4.2.3-1) unstable; urgency=medium

  * New upstream release (Closes: #749135).

 -- Bertrand Marc <beberking@gmail.com>  Sun, 13 Jul 2014 12:15:21 +0200

playonlinux (4.2.2-1) unstable; urgency=medium

  [ Evgeni Golov ]
  * Correct Vcs-* URLs to point to anonscm.debian.org

  [ Jackson Doak ]
  * debian/control: Bump curl from Suggests to Depends (Closes: #716889).

  [ Bertrand Marc ]
  * New upstream release.
  * Provide a complete menu file:
    + build-depend on imagemagick.
    + use convert to build a xpm icon in override_dh_auto_build-indep.
    + copy the xpm icon to /usr/share/pixmaps/.
    + add an icon entry in the menu file (Closes: #727030).
  * Standards version 3.9.5: no changes needed.
  * Add a dependency on netcat, as playonlinux will fail without nc
    (Closes: #710753), on p7zip-full (Closes: #725241) and on bzip2
    (Closes: #725460).
  * debian/rules: make bash/find_python executable. Upstream doesn't seem to
    understand the concept of permissions... Maybe someday.

 -- Bertrand Marc <beberking@gmail.com>  Sat, 25 Jan 2014 13:31:25 +0100

playonlinux (4.2.1-1) unstable; urgency=low

  * New upstream release (closes: #687207, #683682).
  * Most of the patches were included upstream under the condition that
    DEBIAN_PACKAGE is set to TRUE. So delete all the patches included
    upstream and replace them with set_debian_env.diff.
  * Refresh debian/patches/remove_binary_plugin.diff and add a description.
  * Update temporary permission fix in debian/rules.
  * Fix spelling error in description and manpage.
  * debian/control: suggests curl and scrot as the screen capture plugin
    requires them (closes: #676687).
  * State in README.Debian that is linux is the only platform supported
    (closes: #699859).
  * Standards-Version 3.9.4.
  * Update the years in debian/copyright and exlain why the icons from the
    Tango project are in the public domain and what it means.

 -- Bertrand Marc <beberking@gmail.com>  Tue, 21 May 2013 21:47:44 +0200

playonlinux (4.1.1-1) unstable; urgency=low

  * New upstream release.
  * Refresh debian/patches.
  * debian/rules: fix permissions... again.

 -- Bertrand Marc <beberking@gmail.com>  Tue, 22 May 2012 20:15:21 +0200

playonlinux (4.0.17-1) unstable; urgency=low

  * New upstream release.
  * Use debhelper v8 and minimal debian/rules.
  * debian/copyright: use machine readable format.
  * playonlinux.install: allow only script plugins. The Capture plugin
    provides binaries.
  * patch plugins/plugins.lst to remove any mention of Capture.
  * Add xterm as an alternative dependency for x-terminal-emulator, as
    it is the default used upstream and to make lintian happy.

 -- Bertrand Marc <beberking@gmail.com>  Sun, 22 Apr 2012 13:01:41 +0200

playonlinux (4.0.16-1) unstable; urgency=low

  * New upstream release.
  * Depends on x11-utils to provide xdpyinfo.
  * Refresh patches.
  * Standards version 3.9.3.

 -- Bertrand Marc <beberking@gmail.com>  Thu, 22 Mar 2012 21:51:56 +0100

playonlinux (4.0.14-1) unstable; urgency=low

  * New upstream release.
  * Add the new TRANSLATORS file to debian/playonlinux.docs.
  * Refresh patches/x-terminal-emulator.diff.
  * Install etc/playonlinux.gpg to verify downloaded scripts.

 -- Bertrand Marc <beberking@gmail.com>  Sat, 10 Dec 2011 15:48:27 +0100

playonlinux (4.0.12-1) unstable; urgency=low

  * New upstream release (Closes: #639558).
  * Remove patches/license.diff, not necessary any more.
  * debian/rules: remove permission fixing.
  * debian/rules: add build-arch and build-indep.
  * Migrate to dh_python2.
  * debian/patches: disable update alert on new release
    (Closes: #639463).

 -- Bertrand Marc <beberking@gmail.com>  Sat, 17 Sep 2011 11:14:44 +0200

playonlinux (3.8.12-1) UNRELEASED; urgency=low

  * New upstream release.
  * Add depends on gnupg, POL uses signed scripts now.
  * Delete patches/bang.diff, not necessary any more.
  * Refresh patches.
  * Disable completely playonlinux-daemon, as it is *partially* removed
    upstream.
  * Disable playonlinux-url_handler since upstream doesn't provide gconf
    schemas. Upstream should definitly think about providing a clean and
    complete tarball.
  * Add the plugins directory, to make them available easily.
  * Depends on icoutils as POL uses it now.
  * Bump Standards-Version to 3.9.2 (no changes needed).

 -- Bertrand Marc <beberking@gmail.com>  Wed, 27 Apr 2011 20:00:23 +0200

playonlinux (3.8.8-1) unstable; urgency=low

  * New upstream release.
  * Refresh patches.
  * Remove lang/locale/templates.
  * Fix permissions in bash/.
  * Remove a typo in python/new_guiv3.py (!).

 -- Bertrand Marc <beberking@gmail.com>  Wed, 26 Jan 2011 11:42:17 +0100

playonlinux (3.8-1) UNRELEASED; urgency=low

  * New upstream release.
  * Refresh patches.
  * Remove temporary fix for ua locale.
  * debian/rules: lang/old doesn't exist anymore.
  * Stop build-depending on quilt, doesn't needed anymore

 -- Bertrand Marc <beberking@gmail.com>  Wed, 25 Aug 2010 10:52:06 +0200

playonlinux (3.7.7-1) UNRELEASED; urgency=low

  * New upstream release.
  * Remove patches/changelog, included upstream.
  * Fix clean in debian/rules to compile twice in a row.
  * Bump Standards-Version to 3.9.1 (no changes needed).
  * Remove README.source, unnecessary with 3.0 (quilt) format.

 -- Bertrand Marc <beberking@gmail.com>  Mon, 23 Aug 2010 09:30:47 +0200

playonlinux (3.7.6-1) unstable; urgency=low

  * New upstream release.
  * Switch to dpkg-source 3.0 (quilt) format.
  * Refresh debian/rules, remove any quilt references.
  * Complete upstream changelog.
  * debian/rules : temporary fix ukrainian locale.

 -- Bertrand Marc <beberking@gmail.com>  Wed, 19 May 2010 14:36:48 +0200

playonlinux (3.7.3-1) unstable; urgency=low

  * New upstream release.
  * Bump Standards-Version to 3.8.4 (no changes needed).

 -- Bertrand Marc <beberking@gmail.com>  Sun, 14 Feb 2010 21:32:26 +0100

playonlinux (3.7.1-1) unstable; urgency=low

  * New upstream release:
    - Added POL_Call to replace tricks lib
    - New polish and german translation
  * Depends on binutils to provide ar (LP: #456276).

 -- Bertrand Marc <beberking@gmail.com>  Thu, 05 Nov 2009 00:45:44 +0100

playonlinux (3.7-1) unstable; urgency=low

  * New upstream release:
    - Right Menu
    - Kill All Apps
    - Possibility to change icon
    - Possibility to open user directory
    - playonlinux --debug option
  * Depends on wine|wine-unstable (Closes: #541325).
  * Bump Standards-Version to 3.8.3 (no changes needed).
  * Refresh patches.
  * Add README.source referring to /usr/share/doc/quilt/README.source.

 -- Bertrand Marc <beberking@gmail.com>  Mon, 05 Oct 2009 11:54:55 +0000

playonlinux (3.6-1) unstable; urgency=low

  * New upstream release :
    - Various bug fixed
    - Winetricks integration
  * Bump Standards-Version to 3.8.2 (no changes needed).
  * Match playonlinux.install with the new source tree.
  * Add a dependency on gettext-base for translation support.

 -- Bertrand Marc <beberking@gmail.com>  Sun, 05 Jul 2009 13:12:29 +0200

playonlinux (3.5-1) unstable; urgency=low

  * New upstream release :
    - Icon changed
    - pol.mulx.net becomes mulx.playonlinux.com
    - English translation improved

 -- Bertrand Marc <beberking@gmail.com>  Sun, 17 May 2009 16:12:01 +0200

playonlinux (3.4-1) unstable; urgency=low

  * Initial release (Closes: #485149)
  * Suggest (and use) ttf-mscorefonts-installer instead
    of downloading directly the fonts.
  * Document how to install fonts in README.Debian.
  * Install locales in /usr/share/locale instead of
    custom directory and apply patch locale.diff to match
    these changes.
  * Replace xterm with x-terminal-emulator, drop the
    xterm dependency.

 -- Bertrand Marc <beberking@gmail.com>  Fri, 13 Mar 2009 02:03:24 +0100

